var data = {socials: [
  {
    icon: 'send',
    title: 'Telegram'
  },
  {
    icon: 'youtube-play',
    title: 'Youtube'
  },
  {
    icon: 'instagram',
    title: 'Instagram'
  },
  {
    icon: 'facebook',
    title: 'Facebook'
  }
]};