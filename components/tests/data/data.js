var data = {tests: [
  {
    photo: 'static/img/content/test-1.jpg',
    title: 'Цветотип и колористика'
  },
  {
    photo: 'static/img/content/test-2.jpg',
    title: 'Форма лица и тип кожи'
  },
  {
    photo: 'static/img/content/test-3.jpg',
    title: 'Тестирование волос'
  },
  {
    photo: 'static/img/content/test-4.jpg',
    title: 'Форма тела и персональная мода'
  },
  {
    photo: 'static/img/content/test-5.jpg',
    title: 'Характер и гармония моды'
  },
  // {
  //   photo: 'static/img/content/test-1.jpg',
  //   title: ''
  // }
]};