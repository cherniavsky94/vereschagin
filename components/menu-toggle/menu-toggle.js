$(function() {
  var button = $('.menu-toggle');

  button.on('click', toggleMenu);

  function toggleMenu() {
    $('.page').toggleClass('page--menu');
    button.toggleClass('menu-toggle--active');
  }
});