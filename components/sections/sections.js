var sections = new Swiper('.sections__slider', {
  speed: 800,
  direction: 'vertical',
  loop: false,
  mousewheel: true,
  keyboard: true,
  hashNavigation: true,
  parallax: true
});

var nav = new Swiper('.sections__nav', {
  slidesPerView: 3,
  speed: 800,
  spaceBetween: 15,
  direction: 'vertical',
  loop: false,
  centeredSlides: true,
  slideToClickedSlide: true
});

sections.on('slideChange', function() {
  nav.slideTo(sections.activeIndex)
});

nav.on('slideChange', function() {
  sections.slideTo(nav.activeIndex)
});